﻿using UnityEngine;
using System.Collections;

public class RenderShow : MonoBehaviour {

    public bool see;//is seen
    public float sWait = 1f;

    private SphereCollider sCol; //sphere collider
    private float ret = 6.4f; //return size
    private float mSize = 1.6f; //main size
    private float fSize = 6.4f;//full size
    private float rate = 5;//rate of size change
    private GameObject[] objs;
    void Awake()
    {
        sCol = this.GetComponent<SphereCollider>();
        sCol.radius = fSize;
        sCol.enabled = false;
        see = false;
    }
    void Update()
    {
        if (Input.GetButtonUp("Listen") && !see)
        {
            Listen();
        }
        if (see)
        {
            fSize = Mathf.MoveTowards(fSize, mSize, rate * Time.deltaTime);
            sCol.radius = fSize;
        }
        if(fSize <= mSize && see)
        {
            see = false;
        }
        if (!see)
        {
            fSize = ret;
            sCol.radius = mSize;
            sCol.enabled = false;
        }
    }
    void Listen()
    {
        see = true;
        if (see)
        {
            sCol.enabled = true;
            StartCoroutine(SightWait());         
        }
    }

    IEnumerator SightWait()
    {
        yield return new WaitForSeconds(sWait);
        sCol.enabled = false;
        see = false;
    }
}
