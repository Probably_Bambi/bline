﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickMove : MonoBehaviour {

    Vector3 v1;
    Vector3 v2;
    Camera cam;
    Vector3 vel = Vector3.zero;
    bool _move;
    // Use this for initialization
    void Start ()
    {
        cam = GameObject.Find("Camera").GetComponent<Camera>();
        Cursor.visible = true;
	}
    float speed;
	// Update is called once per frame
	void Update ()
    {
        speed = .5f * Time.deltaTime;
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out hit))
            {             
                if (hit.transform.tag == "Floor")
                {
                    Debug.Log(hit.transform.name);
                    v1 = transform.position;
                    v2 = new Vector3(hit.transform.position.x, 1, hit.transform.position.z);
                }
            }
        }
        if (v1 != v2)
        {
            transform.position = Vector3.Lerp(v1, v2, speed);
        }        
    }
}
