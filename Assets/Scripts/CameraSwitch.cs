﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSwitch : MonoBehaviour
{
    GameObject[] Cams;
    UIScript uI;
    List<Transform> CamPos = new List<Transform>();
    int i = 0;
    int _c;
    private void Awake()
    {
        uI = GameObject.Find("UICanvas").GetComponent<UIScript>();
        Cams = GameObject.FindGameObjectsWithTag("SecuritySpots");
        gameObject.transform.SetParent(Cams[0].transform);
        gameObject.transform.position = Cams[0].transform.position;
        gameObject.transform.rotation = Cams[0].transform.rotation;
        foreach (GameObject cP in Cams)
        {
            CamPos.Add(cP.transform);
        }
        _c = CamPos.Count;
        uI.cText = GameObject.Find("CameraText").GetComponent<Text>();
        uI.CameraSwitch(i);
    }
    void Update()
    {
        int _i;
        if (int.TryParse(Input.inputString, out _i))
        {
            foreach (char c in Input.inputString)
            {
                int g;
                string s = c.ToString();
                Int32.TryParse(s, out g);
                if (g == 0)
                {
                    i = 9;
                }
                else if (g == 1)
                {
                    i = 0;
                }
                else if (g >= 2)
                {
                    Int32.TryParse(s, out i);
                    i -= 1;
                }
                MoveCamera(i);
            }
            
        }
        else
            return;
        /*
         * change camera key
                if (Input.GetKeyUp(KeyCode.R))
                {
                    i++;
                    MoveCamera(i);
                }
        */
                
    }
    void MoveCamera(int i)
    {
        gameObject.transform.SetParent(Cams[i].transform);
        gameObject.transform.position = Cams[i].transform.position;
        gameObject.transform.rotation = Cams[i].transform.rotation;
        uI.CameraSwitch(i);
    }
}