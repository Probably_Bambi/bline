using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public Vector3 moveDirection = Vector3.zero;
    public bool move;
    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            if (Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Vertical") > 0)
            {
                move = true;
            }
            else
                move = false;            
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

            if(Input.GetAxis("RotateRight")>0)
            {
                transform.Rotate(new Vector3(0,Input.GetAxis("RotateRight") * 2.5f,0));
            }
            if (Input.GetAxis("RotateLeft") > 0)
            {
                transform.Rotate(new Vector3(0, -Input.GetAxis("RotateLeft") * 2.5f, 0));
            }
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
}