﻿using UnityEngine;
using System.Collections;

public class Sight : MonoBehaviour
{
    public float wait = 1.5f;
    private RenderShow rShow;
    Renderer rend;
    bool show;
    void Awake()
    {
        rend = GetComponent<Renderer>();
        rend.enabled = false;
    }
  void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && !show && rend.enabled == false)
        {
            rShow = col.GetComponent<RenderShow>();
            show = true;
            if (rShow.see == true)
            {
                rend.enabled = true;
                float x;
                x = rShow.sWait;
                StartCoroutine(ShowWait(x));
            }
        }
    }
    void OnTriggerExit(Collider col)
    {
         rend.enabled = false;
    }
    IEnumerator ShowWait(float x)
    {
        yield return new WaitForSeconds(x);
        rend.enabled = false;
        show = false;
    }
}
