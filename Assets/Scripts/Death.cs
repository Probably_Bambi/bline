﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour {

    // Use this for initialization
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Player")
        {
            UIScript ui = GameObject.Find("UICanvas").GetComponent<UIScript>();
            ui.Reset(0);
            ui.ded.enabled = true;
        }
    }
}
