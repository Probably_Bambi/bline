﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class UIScript : MonoBehaviour {
    private Canvas uCan;
    public Text cText;//camera text
    private CamMove Camm;
    public Text ded;
	// Use this for initialization
	void Awake () {

        ded = GameObject.Find("dText").GetComponent<Text>();
//        Camm = GameObject.Find("SecurityController").GetComponent<CamMove>();
        uCan = this.GetComponent<Canvas>();
        ded.enabled = false;
        Cursor.visible = false;
    }
  public void CameraSwitch(int cam)
    {
        //Debug.Log(cam);
        //uCan.worldCamera = Camm.security.GetComponentInParent<Camera>();
        cam++;
        string camN = cam.ToString();
        cText.text = "Feed # " + camN;
    }
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetButtonUp("Escape"))
        {
            Cursor.visible = !Cursor.visible;
            CamMove c = GameObject.FindGameObjectWithTag("Player").GetComponent<CamMove>();
            c.enabled = !c.enabled;
//          RenderShow rend = GameObject.Find("Seer").GetComponent<RenderShow>();
//         rend.enabled = false;
        }
	}
    public void Reset(int lvl)
    {
        StartCoroutine(ResetWait(lvl));
    }
    IEnumerator ResetWait(int lvl)
    {
        yield return new WaitForSeconds(.5f);
       Debug.Log(lvl);
        SceneManager.LoadScene(lvl);
        ded.enabled = false;
    }
}
