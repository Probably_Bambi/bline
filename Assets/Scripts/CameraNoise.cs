﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraNoise : MonoBehaviour {

    public Material NoiseMaterial;
    public int NoiseSize = 512;
    public float NoiseTick = 0.030f;
    Texture2D NoiseTexture;
    Camera Camera;

    float Timer;

	// Use this for initialization
	void Start () {
        // If we haven't specified a noise tex just generate one
        if(NoiseTexture == null)
            NoiseTexture = GenerateRandomizedNoise(NoiseSize);

        Camera = GetComponent<Camera>();

        // Make camera generate depth buffers if it doesn't already
        if (Camera.depthTextureMode != DepthTextureMode.None)
            Camera.depthTextureMode = DepthTextureMode.Depth;


        NoiseMaterial.SetTexture("_NoiseTex", NoiseTexture);

        Timer = Time.time;

    }
	
	// Update is called once per frame
	void Update () {
		if(Time.time > Timer)
        {
            NoiseMaterial.SetVector("_NoiseOffset", new Vector2(1f - (Random.value * 2f), 1f - (Random.value * 2f)));
            Timer = Time.time + NoiseTick;
        }
	}

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        Graphics.Blit(src, dest, NoiseMaterial);
    }

    static Texture2D GenerateRandomizedNoise(int size)
    {
        // Make a texture with a bunch of random black and white
        // pixolas. 
        Texture2D tex = new Texture2D(size, size);

        Color[] pixels = new Color[size * size];

        for(int i = 0; i < pixels.Length; i++)
        {
            if (Random.value >= 0.5f)
                pixels[i] = Color.black;
            else
                pixels[i] = Color.white;
        }

        tex.SetPixels(pixels);
        tex.Apply();

        return tex;
    }
}