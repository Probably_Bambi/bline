﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Post Process/CamNoiseShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_NoiseStrength("Noise Strength", Float) = 0.09
		_NoiseColorBlend("Noise Color", Color) = (1,1,1,1)
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			fixed2 _NoiseOffset;
			fixed _NoiseStrength;
			fixed4 _NoiseColorBlend;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _NoiseTex;
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 noiseColor = (tex2D(_NoiseTex, i.uv + _NoiseOffset) * _NoiseColorBlend) * _NoiseStrength;
				noiseColor.a = 0;

				fixed4 col = tex2D(_MainTex, i.uv);
		
				return col + noiseColor;
			}
			ENDCG
		}
	}
}